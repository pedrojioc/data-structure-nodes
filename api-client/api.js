const API_BASE = 'http://localhost/lib/controlador_binario.php';
class ApiBinario {
  constructor() {

  }
  async crearRaiz(nombre) {
    let url = API_BASE+'?accion=crearRaiz&raiz='+nombre
    console.log('ejecutando request')
    try {
      let response = await fetch(url, {
        method: 'get',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
      })
      let responseJson = await response.json()
      console.log('resolve')
      return responseJson
    } catch (e) {
      console.error('Ha ocurrido un error: '+e)
      return
    }
  }

  async crearNodo(data) {
    let params = Object.entries(data).map(([key, val]) => `${key}=${val}`).join('&');
    //let uri = encodeURIComponent(params)
    let url = API_BASE+'?'+params

    console.log('ejecutando request crear nodo')
    try {
      let response = await fetch(url, {
        method: 'get',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
      })
      let responseJson = await response.json()
      console.log('resolve crear nodo')
      return responseJson
    } catch (e) {
      console.error('Ha ocurrido un error: '+e)
      return
    }
  }

  async getNodos() {

    let url = API_BASE+'?accion=getNodos'

    console.log('ejecutando request consultando nodos')
    try {
      let response = await fetch(url, {
        method: 'get',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
      })
      let responseJson = await response.json()
      console.log('resolve get nodos')
      return responseJson
    } catch (e) {
      console.error('Ha ocurrido un error: '+e)
      return
    }
  }

  async eliminarNodo(nodoId) {

    let url = API_BASE+'?accion=eliminarNodo&nodoId='+nodoId

    console.log('ejecutando request eliminar nodo')
    try {
      let response = await fetch(url, {
        method: 'get',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
      })
      let responseJson = await response.json()
      console.log('resolve del nodos')
      return responseJson
    } catch (e) {
      console.error('Ha ocurrido un error: '+e)
      return
    }
  }

  async contarNodos() {

    let url = API_BASE+'?accion=contarNodos'

    console.log('ejecutando request eliminar nodo')
    try {
      let response = await fetch(url, {
        method: 'get',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
      })
      let responseJson = await response.json()
      console.log('resolve del nodos')
      return responseJson
    } catch (e) {
      console.error('Ha ocurrido un error: '+e)
      return
    }
  }

} //End Class





// return new Promise( (resolve, reject) => {
//   let response = fetch(API_BASE, {
//     method: 'POST',
//     headers: {
//       'Accept': 'application/json',
//       'Content-Type': 'application/json'
//     },
//     body: JSON.stringify({
//       action: 'crearRaiz',
//       raiz: nombre
//     })
//   }).then( res => {
//     resolve(res)
//   } )
//   .catch(err => reject(err))
// })
