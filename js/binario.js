//Config
const OPTIONS = {
  autoResize: true,
  nodes: {
    shape: 'circle',
    size: 30,
     font: {
       size: 20,
       color: '#ffffff',
     },
    borderWidth: 2,
  },
  edges: {
    smooth: {
              type: 'cubicBezier',
              forceDirection: 'horizontal',
              roundness: 0.4
            },
    width: 2
  },

  layout: {
    hierarchical: {
      direction: 'UD',
      sortMethod: 'directed',
    }
  },
  physics: {
    stabilization: false
  }

}




const CONTAINER = document.getElementById('network')
const BTN_CREAR_RAIZ = document.querySelector('#btn_crear_raiz')
const BTN_CREAR_NODO = document.querySelector('#btn_crear_nodo')
const BTN_ELIMINAR_NODO = document.querySelector('#btn_eliminar_nodo')
const BTN_CONTAR_NODOS = document.querySelector('#btn_contar_nodos')


const RAIZ = document.getElementById('raiz')
const POSICION = document.getElementById('nodo_posicion')
const PADRE = document.getElementById('nodo_padre')
const HIJO = document.getElementById('nodo_hijo')
const NODO_ELIMINAR = document.getElementById('nodo_eliminar')
const API = new ApiBinario
class Binario {
  constructor() {
    this.nodes = []
    this.inicializar()
  }

  inicializar() {
    this.handleCrearRaiz = this.handleCrearRaiz.bind(this)
    this.handleCrearNodo = this.handleCrearNodo.bind(this)
    this.handleEliminarNodo = this.handleEliminarNodo.bind(this)
    this.handleContarNodos = this.handleContarNodos.bind(this)
    this.addEvents()
    this.iniciarArbol()

  }

  addEvents() {
    BTN_CREAR_RAIZ.addEventListener('click', this.handleCrearRaiz)
    BTN_CREAR_NODO.addEventListener('click', this.handleCrearNodo)
    BTN_ELIMINAR_NODO.addEventListener('click', this.handleEliminarNodo)
    BTN_CONTAR_NODOS.addEventListener('click', this.handleContarNodos)
  }
  iniciarArbol() {
    this.getNodos()
  }

  async handleContarNodos( event ) {
    let rs = await API.contarNodos()
    swal ( `${rs}` ,  "Nodo(s) contiene el arbol", "success" )
  }
  async getNodos() {
    let rs = await API.getNodos()
    this.nodes = rs
    this.construirArbol()
  }
  async handleCrearRaiz( event ) {
    let nombre = RAIZ.value
    let rs = await API.crearRaiz(nombre)

    this.nodes = rs
    this.construirArbol()
    RAIZ.value = ''
  }
  async handleCrearNodo( event ) {
    if( !this.verificarNodo() ) return
    let data = {
      accion: 'crearNodo',
      nodoPadre: PADRE.value,
      nodoPosicion: POSICION.value,
      nodoHijo: HIJO.value
    }

    console.log('handle crear nodo')
    let rs = await API.crearNodo(data)

    this.nodes = rs
    this.construirArbol()
    this.limpiarCampos()
  }
  async handleEliminarNodo( event ) {
    if( !this.verificarEliminarNodo() ) return
    let rs = await API.eliminarNodo(NODO_ELIMINAR.value)
    this.nodes = rs
    this.construirArbol()
    NODO_ELIMINAR.value = ''
  }
  verificarEliminarNodo() {
    let nodo = this.buscarNodo(NODO_ELIMINAR.value)
    if(nodo !== null ) {
      if(nodo.hijo_izquierda.length > 0 || nodo.hijo_derecha.length > 0) {
        swal ( 'Error',  'El nodo no puede ser eliminado. Contiene nodos hijos', 'error')
        return false
      }
    } else {
      alert('')
      swal ( 'Importante',  'El nodo no existe', 'info')
      return false
    }

    return true
  }
  verificarNodo() {
    let existePadre = 0
    if(PADRE.value === '' || HIJO.value === '') {
      swal ( 'Importante',  'Completa los campos para crear el nodo', 'info')
      return false
    }
    if(POSICION.value === 'none') {
      swal ( 'Importante',  'Selecciona una posición', 'info')
      return false
    }

    if(this.buscarNodo(HIJO.value) !== null) {
      swal ( 'Error',  'El nodo ya existe, ingresa uno diferente', 'error')
      return false
    }

    let nodoPadre = this.buscarNodo(PADRE.value)
    if(nodoPadre !== null) {
      existePadre = 1
      if(POSICION.value === 'left' && nodoPadre.hijo_izquierda.length > 0) {
        let nodoHijo = this.buscarNodo(nodoPadre.hijo_izquierda)
        if(nodoHijo.hijo_izquierda.length > 0 || nodoHijo.hijo_derecha.length > 0) {
          swal ( 'Error',  'No es posible crear el nodo, existe un nodo en esta posición que contiene nodos hijos', 'error')
          return false
        }
      } else if(POSICION.value === 'right' && nodoPadre.hijo_derecha.length > 0) {
        let nodoHijo = this.buscarNodo(nodoPadre.hijo_derecha)
        if(nodoHijo.hijo_izquierda.length > 0 || nodoHijo.hijo_derecha.length > 0) {
          swal ( 'Error',  'No es posible crear el nodo, existe un nodo en esta posición que contiene nodos hijos', 'error')
          return false
        }
      }
    }

    if(existePadre === 0) {
      swal ( 'Error',  'El nodo padre no existe', 'error')
      return false
    }

    return true
  }

  construirArbol() {
    if (this.nodes.length === 0) return
    let edges = []
    for(let node of this.nodes) {
      if(node.hijo_izquierda.length !== 0) edges.push({from: node.id, to: node.hijo_izquierda})
      if(node.hijo_derecha.length !== 0) edges.push({from: node.id, to: node.hijo_derecha})
    }

    console.log(this.nodes)
    console.log(edges)

    let data = {
      nodes: this.nodes,
      edges: edges
    }
    this.network = new vis.Network(CONTAINER, data, OPTIONS);
  }

  limpiarCampos() {
    PADRE.value = ''
    HIJO.value = ''
    POSICION.selectedIndex = '0'
  }
  buscarNodo(nodoId) {
    let rs = null
    for(let node of this.nodes) {
      if(node.id === nodoId) {
        rs = node
        break
      }
    }
    return rs
  }

}
