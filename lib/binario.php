<?php
class Binario {

  private static $db = "../db/data.json";

  public function __construct() {
    $this->padre_id = null;
    $this->posicion = null;
    $this->nombre = null;
    $this->hijo_izquierda = "";
    $this->hijo_derecha = "";
    $this->nodos = json_decode(file_get_contents(self::$db));
  }

  //Setters
  public function setPadreId($padre_id) {
    $this->padre_id = $padre_id;
  }
  public function setPosicion($posicion) {
    $this->posicion = $posicion;
  }
  public function setNombre($nombre) {
    $this->nombre = $nombre;
  }
  public function setHijoIzquierda($nodo_id) {
    $this->hijo_izquierda = $nodo_id;
  }
  public function setHijoDerecha($nodo_id) {
    $this->hijo_derecha = $nodo_id;
  }

  //Getters
  public function getNodos() {
    return json_encode($this->nodos);
  }


  public function crearRaiz() {
    $rs = false;

    $data = [
      [
        "id" => $this->nombre,
        "nombre" => $this->nombre,
        "label" => $this->nombre,
        "padre_id" => $this->nombre,
        "posicion" => $this->posicion,
        "hijo_izquierda" => $this->hijo_izquierda,
        "hijo_derecha" => $this->hijo_derecha
      ]
    ];

    $data_encode = json_encode($data);
    if(file_put_contents(self::$db, $data_encode) !== false) {
      $rs = $data_encode;
    }

    return $rs;

  }
  public function crearNodo() {
    $nodo_padre = $this->buscarNodo($this->padre_id);
    if($this->posicion === "left") {
      if( strlen($nodo_padre->hijo_izquierda) > 0 ) {
        $nodo_hijo_actual = $this->buscarNodo($nodo_padre->hijo_izquierda);
        $nodo_hijo_actual->padre_id = $this->$nombre;
        $this->hijo_izquierda = $nodo_hijo_actual->id;
      }
      $nodo_padre->hijo_izquierda = $this->nombre;
    } else {
      if( strlen($nodo_padre->hijo_derecha) > 0 ) {
        $nodo_hijo_actual = $this->buscarNodo($nodo_padre->hijo_derecha);
        $nodo_hijo_actual->padre_id = $this->$nombre;
        $this->hijo_derecha = $nodo_hijo_actual->id;
      }
      $nodo_padre->hijo_derecha = $this->nombre;
    }

    //Creamos un nuevo objeto (Nuevo nodo)
    $data = [
      "id" => $this->nombre,
      "nombre" => $this->nombre,
      "label" => $this->nombre,
      "padre_id" => $this->padre_id,
      "posicion" => $this->posicion,
      "hijo_izquierda" => $this->hijo_izquierda,
      "hijo_derecha" => $this->hijo_derecha
    ];
    $data = ( object ) $data;

    array_push($this->nodos, $data);

    $rs = $this->guardar();

    return $rs;
  }

  public function eliminarNodo($nodo_id) {

    foreach ($this->nodos as $key => $nodo) {
      if($nodo->id === $nodo_id) {
        array_splice($this->nodos, $key);
        break;
      }
    }
    $rs = $this->guardar();

    return $rs;
  }

  public function contarNodos($i = 0) {

    if (!empty($this->nodos[$i])) {
      return $this->contarNodos($i+1);
    } else {
      return $i;
    }
  }

  private function guardar() {
    $rs = false;
    $json_nodos = json_encode($this->nodos);

    if(file_put_contents(self::$db, $json_nodos) !== false) {
      $rs = $json_nodos;
    }

    return $rs;
  }

  private function buscarNodo($nodo_id) {
    $rs = null;
    foreach ($this->nodos as $key => $nodo) {
      if($nodo->id == $nodo_id){
        $rs = $nodo;
        break;
      }
    }
    unset($nodo);
    return $rs;
  }

}

//Binario::getNodos();
// $binario = new Binario();
// $rs = $binario->getNodos();
// echo $rs;
?>
