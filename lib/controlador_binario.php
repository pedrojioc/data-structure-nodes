<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if($method == "OPTIONS") {
  die();
}

require 'binario.php';
class ControladorBinario {

  public function getNodos() {
    $binario = new Binario();
    $rs = $binario->getNodos();
    echo $rs;
  }

  public function crearRaiz($nombre) {
    // $rs = Binario::crearRaiz($nombre);
    // echo $rs;
    $binario = new Binario();
    $binario->setPadreId($nombre);
    $binario->setPosicion("raiz");
    $binario->setNombre($nombre);
    $rs = $binario->crearRaiz();
    echo $rs;
  }
  public function crearNodo($padre, $posicion, $nombre) {
    $binario = new Binario();
    $binario->setPadreId($padre);
    $binario->setPosicion($posicion);
    $binario->setNombre($nombre);
    $rs = $binario->crearNodo();
    echo $rs;
  }
  public function eliminarNodo($nodo_id) {
    $binario = new Binario();
    $rs = $binario->eliminarNodo($nodo_id);
    echo $rs;
  }

  public function contarNodos() {
    $binario = new Binario();
    $rs = $binario->contarNodos();
    echo $rs;
  }
}

$controlador = new ControladorBinario;
//Router

switch ($_GET['accion']) {
  case 'crearRaiz':
    $controlador->crearRaiz($_GET['raiz']);
    break;
  case 'crearNodo':
    $controlador->crearNodo($_GET['nodoPadre'], $_GET['nodoPosicion'], $_GET['nodoHijo']);
    break;
  case 'eliminarNodo':
    $controlador->eliminarNodo($_GET['nodoId']);
    break;
  case 'getNodos':
    $controlador->getNodos();
    break;
  case 'contarNodos':
    $controlador->contarNodos();
    break;
  default:
    // code...
    break;
}
?>
