<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Arbol binario</title>

    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/vis.min.css">
    <style media="screen">
      #network {
        width: 100%;
        height: 500px;
        background-color: white;
        border: 2px solid #d2d5da;
        border-radius: 12px;

      }
      .title {
        font-weight: 300;
        line-height: 1.2;
      }
    </style>
  </head>
  <body>
    <header>
      <nav>
        <ul class="nav justify-content-center">
          <li class="nav-item">
            <a class="nav-link active" href="#">José Armando Ortega Rada</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Mac Perez Zambrano</a>
          </li>

        </ul>
      </nav>
    </header>
    <div class="container">

      <div class="row justify-content-center">
        <div class="col-sm-10 text-center">
          <h1 class="title">Arbol Binario</h1>
        </div>
      </div>
      <br><br>
      <div class="row">
        <div class="col-sm-12 col-md-5">
          <div class="row">
            <div class="col-sm-12">
              <label>Crear Arbol</label>
            </div>
          </div>
          <!-- Crear Raiz -->
          <div class="row">
            <div class="col-sm-12">
              <form class="form-inline" id="crear_raiz">
                <input type="hidden" name="action" value="crearRaiz">
                <div class="form-group mr-2 mb-2">
                  <input type="text" class="form-control" id="raiz" name="raiz" placeholder="Nombre de la raíz">
                </div>
                <button type="button" id="btn_crear_raiz" class="btn btn-outline-primary mb-2">Crear Arbol</button>
              </form>
            </div>
          </div>
          <!-- ./Crear Raiz -->
          <hr>
          <!-- Crear Nodo -->
          <div class="row">
            <div class="col-sm-12">
              <form class="form-inline" id="crear_nodo">
                <input type="hidden" name="action" value="crearNodo">
                <div class="form-group mr-2 mb-2">
                  <input type="text" class="form-control" id="nodo_padre" name="nodo_padre" placeholder="Nodo padre">
                </div>
                <div class="form-group mr-2 mb-2">
                  <select class="custom-select mr-sm-2" id="nodo_posicion" name="nodo_posicion">
                    <option value="none"selected>Posición</option>
                    <option value="left">Izquierda</option>
                    <option value="right">Derecha</option>
                  </select>
                </div>
                <div class="form-group mr-2 mb-2">
                  <input type="text" class="form-control" id="nodo_hijo" name="nodo_hijo" placeholder="Nodo hijo">
                </div>
                <button type="button" id="btn_crear_nodo" class="btn btn-outline-primary mb-2">Crear nodo</button>
              </form>
            </div>
          </div>
          <!-- ./Crear Nodo -->
          <hr>
          <!-- Eliminar Nodo -->
          <div class="row">
            <div class="col-sm-12">
              <form class="form-inline" id="eliminar_nodo">
                <input type="hidden" name="action" value="eliminarNodo">
                <div class="form-group mr-2 mb-2">
                  <input type="text" class="form-control" id="nodo_eliminar" name="nodo_eliminar" placeholder="Nombre nodo">
                </div>
                <button type="button" id="btn_eliminar_nodo" class="btn btn-outline-primary mb-2">Eliminar nodo</button>
              </form>
            </div>
          </div>
          <!-- ./Eliminar Nodo -->
          <hr>
          <!-- Operaciones -->
          <div class="row">
            <div class="col-sm-12">
              <form class="form-inline">

                <button type="button" id="btn_contar_nodos" class="btn btn-outline-primary mb-2">Contar nodos</button>
              </form>
            </div>
          </div>
          <!-- ./Operaciones -->
        </div>
        <div class="col-sm-12 col-md-7">
          <div id="network">

          </div>
        </div>
      </div>
    </div>
    <script type="text/jscript" src="./api-client/api.js"></script>
    <script type="text/javascript" src="./js/vis.min.js"></script>
    <script type="text/javascript" src="./js/sweetalert.min.js"></script>
    <script type="text/javascript" src="./js/binario.js"></script>
    <script type="text/javascript">

      var binario = new Binario
    </script>
  </body>

</html>
